import {AuthPageComponent} from '../pages/auth-page/auth-page.component';

export const appRoutes = [
  {
    path: 'auth',
    component: AuthPageComponent
  },
  {
    path: '**',
    redirectTo: 'auth'
  }
];
