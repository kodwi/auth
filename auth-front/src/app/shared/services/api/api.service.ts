import {Injectable} from '@angular/core';
import {IAuthData, IVkParams} from '../../app-interfaces';
import {Observable, of, throwError} from 'rxjs';
import {delay, switchMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _correctAuthData: IAuthData = {
    login: '+79991234567',
    password: '-0987654321'
  };

  private _vkParams: IVkParams = {
    clientId: 6678149,
    redirectUri: 'http://localhost:4200/auth'
  };


  authByDefault(authData: IAuthData): Observable<any> {
    return of(null).pipe(
      delay(Math.floor(Math.random() * 5000)),
      switchMap(() =>
        this._correctAuthData.login === authData.login &&
        this._correctAuthData.password === authData.password ?
          of('You are logged in') : throwError('Incorrect auth data'))
    );
  }

  authByVk() {
    window.location.href = `https://oauth.vk.com/authorize?client_id=${this._vkParams.clientId}&display=page&redirect_uri=${this._vkParams.redirectUri}&scope=friends&response_type=token&v=5.84&state=success`;
  }
}
