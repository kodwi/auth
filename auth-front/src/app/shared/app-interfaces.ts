export interface IAuthData {
  login?: string;
  password?: string;
}

export interface IVkParams {
  clientId?: number;
  redirectUri?: string;
}
