import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthTypeEnum} from '../../app-enums';
import {ApiService} from '../../services/api/api.service';
import {IAuthData} from '../../app-interfaces';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-auth-form',
  templateUrl: './auth-form.component.html',
  styleUrls: ['./auth-form.component.css']
})
export class AuthFormComponent implements OnInit {
  authForm: FormGroup;
  loginControl: FormControl;
  passwordControl: FormControl;
  currAuthType: AuthTypeEnum;

  authTypeEnum = AuthTypeEnum;


  constructor(private readonly _api: ApiService) {
  }

  ngOnInit() {
    this.authForm = new FormBuilder().group({
      login: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });

    this.loginControl = this.authForm.get('login') as FormControl;
    this.passwordControl = this.authForm.get('password') as FormControl;
  }


  markAllFieldsAsTouched(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(controlName => {
      const currControl = formGroup.get(controlName);

      if (currControl instanceof FormControl) {
        currControl.markAsTouched();
      } else if (currControl instanceof FormGroup) {
        this.markAllFieldsAsTouched(currControl);
      }
    });
  }

  auth() {
    const authData = Object.assign({}, this.authForm.value) as IAuthData;

    switch (this.currAuthType) {
      case AuthTypeEnum.Default: {
        this.authForm.disable();

        this._api.authByDefault(authData).pipe(
          finalize(() => this.authForm.enable())
        )
          .subscribe(
            (success: string) => console.log(success),
            (error: any) => console.error(error)
          );

        break;
      }

      case AuthTypeEnum.Vk: {
        this._api.authByVk();

        break;
      }
    }
  }
}
