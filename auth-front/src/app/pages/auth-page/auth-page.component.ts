import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth-page.component.html',
  styleUrls: ['./auth-page.component.css']
})
export class AuthPageComponent {
  constructor(private readonly _route: ActivatedRoute) {
    this._route.fragment.subscribe(result => {
      if (!result) {
        return;
      }

      const splittedData = result.split('&');

      splittedData.forEach(item => console.log(item));
    });
  }
}
