import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {appRoutes} from './shared/app-routes';

import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {AuthPageComponent} from './pages/auth-page/auth-page.component';
import {AuthFormComponent} from './shared/components/auth-form/auth-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ApiService} from './shared/services/api/api.service';

@NgModule({
  declarations: [
    AppComponent,
    AuthPageComponent,
    AuthFormComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule
  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
